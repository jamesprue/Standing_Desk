**Step 1. Research for ideas**
- Watched videos about how height adjustable things work
- https://www.youtube.com/watch?v=NSRXYPe2ztY
- https://www.youtube.com/watch?v=X4-yOB3qFKI
- http://kevinjantzer.com/diy-adjustable-standing-desk/
- https://www.youtube.com/watch?v=ohyMRATOt5U
- https://www.youtube.com/watch?v=DtBSwr603sA

**Step 2.  Scratching my ideas out on paper**
- 1st thought: Where do I start?
- 2nd thought: Maybe I should just buy an adjustable desktop
- Balancing design with functionality
- Seeing designs take shape made me realize the design errors that weren't apparent in my mind
- I'm glad I drew in pencil

<img src="Pictures/20180329_154258.jpg" width=800px>
<img src="Pictures/20180329_145632.jpg" width=800px>
    


**Step 3. Learning the design software "Autodesk Fusion 360"**
- Observations
    - Steep learning curve for someone without a CAD or Design background
    - Not self explanatory
    - Millions of mistakes ('control + z' and I are now best friends)
    - After the initial frusteration it became fun and addicting
    - After the two hours it took me to design 5 relatively simple parts I felt like I could navigate Fusion 360 relatively well on this simple level
    
<img src="Pictures/Fusion_360.jpg" width=800px>

**Step 4. Prototyping**
- Attempt 1
    - What process would I use?
        - 3d printing
        - Laser Cutting- I later decided on this one after Sam and Will's recommendation
        - Waterjet
        - Shop Bot
    - What material would I use?
        - John suggested wood and trying the waterjet as a cool process
        - Will suggest 2.5mm thick Delrin with the laser cutter which I eventually settled on
    - Using Corel Draw
        - Uploaded my files
        - Chose 1:2 as the scale
        - Verified hairline and other settings
    - Using the Epilog Legend 36EXT
        - Got some additional training from Tom since I had only done Raster on Glass mugs before
        - Tom wasn't 100% on the Power and Speed settings so he started with Speed 5 and Power 70
    - Below is the result of the first attempt

<img src="Pictures/20180329_150021.jpg" width=800px>      

- Attempt 2
    - Things to figure out before trying again
        - Why did my parts come out so small?  I designed the desktop piece at half scale so I thought that the Corel Draw 1:2 setting would be correct
            - I talked to Sam and he said that many programs don't play well together so there's some work to do to figure out the settings
            - After measuring various feature sizes on the tiny part compared to what I wanted I decided to enlarge everything by 500%.  It worked!  I still don't know why but
            it worked.  It shouldn't have worked because I doubled the size originally with the scale setting in Corel draw then multiplied it by 5 again later.  That means
            the parts we 10 times the size I designed them at by the actual size doesn't line up with the measurements of my designs in Fusion 360
        - Why did so many of my parts not come out at all?
            - I looked at the sheet of Delrin and found that most of my parts were so small that the laser just melted the material instead of cutting pieces out.  
            - I needed to adjust the Speed and Power settings on the laser.  After asking Tom he said it's a bit of guess and check.  I decided to try Speed 10 Power 40.
    - I made a second attempt and the below pictures are the result
<img src="Pictures/20180329_114937.jpg" width=800px>   
<img src="Pictures/20180329_120013.jpg" width=800px> 
<img src="Pictures/20180329_150654.mp4" width=800px>    
- Attempt 3
    - Things to figure out, change, fix, redesign before trying again
        - The notches for the cross piece to slide into that would in turn hold the desktop in position were on the bottom... they need to be on the top.  Also too narrow.
            - Will fix in Fusion 360
        - For stability the framework of both the base as well as the pieces attached to the bottom of the desktop should be wider for more stability
            - Will measure and redesign in Fusion 360
        - When the desktop is raised up completely and the two scissor arms come too close together the whole desktop falls forward to about -45 degrees. Ben called this a 
        problem of singularity
            - I'll redesign the lower track so that there's not as much travel and the arms stay closer to an 'X' shape
            - Maybe one of the arms need to be longer?  Must explore
        - A small problem but just for a bonus I want to make the tabs coming up through the desktop flush.  My full scale design won't include this feature but want to 
        attempt anyways
    - I made a 3rd attempt and the 2nd prototype is below in pictures
<img src="Pictures/2_top_view.jpg" width=800px>   
<img src="Pictures/2_closeup.jpg" width=800px> 
<img src="Pictures/2_side_view.jpg" width=800px> 